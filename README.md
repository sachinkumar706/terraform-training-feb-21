# Terraform Training Feb 2021
Welcome to Terraform training. 
Exercises are testing on  Terraform version 0.14.3

## Course Content
[Terraform Course Content](CourseContent.md)

## Presentations
[Terraform Course Presentations](https://1drv.ms/u/s!AitfmUcWnxXYhXovWz2T-AHks39M?e=kEbBo7)

## Terraform Installation
[Install Terraform](Install-Terraform.md)

## File Server Access (Optional)
[storage-account-connect.md](storage-account-connect.md)

## Tear Down !!!
When you complete an exercise, be sure to tear down the infrastructure.  Each exercise file ends with `terraform destroy`.  Just run that command and approve the destruction to remove all resources from AWS.

## Useful Commands and examples
[Useful Commands](useful-commands.md)
