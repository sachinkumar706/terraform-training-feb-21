# **Course Introduction**

Terraform is server provisioning tool which is used for developing, changing and versioning infrastructure safely and precisely. Terraform has become favored because it uses simple syntax which allows easy customization and works against multi-cloud. Terraform is written in Go language and it is an open source tool.

Benefits:

- Technical benefits: Less complex problems to fix
- Technical benefits: Faster resolution of problems
- Business benefits: Faster delivery of features
- Business benefits: More stable operating environments
- Business benefits: More time available to add value (rather than fix/maintain)

Who Should Enroll?

This course is a foundation to anyone who aspires to become a DevOps Engineer, a Service Engineer in the field of Enterprise Infrastructures. The following professionals are the key beneficiaries of this course:

- DevOps Engineer
- Build and Release Engineer
- AppOps Engineer
- Site Reliability Engineer
- System Administrator
- Operations Engineer
- Automation Engineer

Training Pre-requisites

- Basic understanding of linux/unix system concepts
- Familiarity with Command Line Interface (CLI)
- Familiarity with a Text Editor
- Experience with managing systems/applications/infrastructure or with deployments/automation
- Familiarity with developing and building software
- Familiarity with Visual Studio 2005, 2008, or 2010
- Familiarity with their organizaiton&#39;s development process
- Be able to read and understand C# and C++ code (all source code will be provided)

#

# **Intermediate**

##### **Day 1 - Deploying Your First Terraform Configuration**

- Introduction
- What&#39;s the Scenario?
- Terraform Components
- Demo Time!

#####


##### **Day 1 - Updating Your Configuration with More Resources**

- Introduction
- Terraform State and Update
- What&#39;s the Scenario?
- Data Type and Security Groups
- Demo Time!

#####


##### **Day 2 - Configuring Resources After Creation**

- Introduction
- What&#39;s the Scenario?
- Terraform Provisioners
- Terraform Syntax
- Demo Time!

#####


#####


##### **Day 2 - Adding a New Provider to Your Configuration**

- Introduction
- What&#39;s the Scenario?
- Terraform Providers
- Terraform Functions
- Demo: Intro and Variable
- Demo: Resource Creation
- Demo: Deployment and Terraform Console
- Demo: Updated Deployment and Terraform Commands

# **advanced**

#####


#####


#####


##### **Day 3 - Using Variables in Deployments**

- Introduction
- What&#39;s the Scenario?
- Working with Variables
- Multiple Environments
- Demo: Multiple Environments
- Demo: Deploying the Dev Environment
- Demo: Deploying the UAT Environment

#####


##### **Day 3 - Using Modules for Common Configurations**

- Introduction
- What&#39;s the Scenario?
- Terraform Modules
- Demo Time!
- Demo: Using the Modules
- Demo: Deploying the Configuration

**Day 4 – Continued..**

- Introduction to VPCs
- Launching EC2 instances in the VPC
- EBS Volumes
- Userdata
- Static IPs, EIPs, and Route53
- RDS
- IAM Roles
- Autoscaling
- Introduction to Elastic Load Balancers (ELB)
- ELBs in terraform
- Application Load Balancer (ALB)

- Elastic Beanstalk
- Demo: Elastic Beanstalk
- Packer Introduction
- Demo- Packer with Terraform
- Terraform with Packer and Jenkins