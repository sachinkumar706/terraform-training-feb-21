# Connect to File Server
 - Please use below instructions to connect to the file server

## Windows (Powershell)
```
$connectTestResult = Test-NetConnection -ComputerName saterraform2.file.core.windows.net -Port 445
if ($connectTestResult.TcpTestSucceeded) {
    # Save the password so the drive will persist on reboot
    cmd.exe /C "cmdkey /add:`"saterraform2.file.core.windows.net`" /user:`"Azure\saterraform2`" /pass:`"PxAV4itPCxLv8OYUJO1kxEsggempScAicI82bLonvAJSoJj1AuJrD9aHZZ/+jjMzFGXKQEhwrwkxuVuMaGoRrg==`""
    # Mount the drive
    New-PSDrive -Name Z -PSProvider FileSystem -Root "\\saterraform2.file.core.windows.net\terraform" -Persist
} else {
    Write-Error -Message "Unable to reach the Azure storage account via port 445. Check to make sure your organization or ISP is not blocking port 445, or use Azure P2S VPN, Azure S2S VPN, or Express Route to tunnel SMB traffic over a different port."
}
```

## Linux
```
sudo mkdir /mnt/terraform
if [ ! -d "/etc/smbcredentials" ]; then
sudo mkdir /etc/smbcredentials
fi
if [ ! -f "/etc/smbcredentials/saterraform2.cred" ]; then
    sudo bash -c 'echo "username=saterraform2" >> /etc/smbcredentials/saterraform2.cred'
    sudo bash -c 'echo "password=PxAV4itPCxLv8OYUJO1kxEsggempScAicI82bLonvAJSoJj1AuJrD9aHZZ/+jjMzFGXKQEhwrwkxuVuMaGoRrg==" >> /etc/smbcredentials/saterraform2.cred'
fi
sudo chmod 600 /etc/smbcredentials/saterraform2.cred

sudo bash -c 'echo "//saterraform2.file.core.windows.net/terraform /mnt/terraform cifs nofail,vers=3.0,credentials=/etc/smbcredentials/saterraform2.cred,dir_mode=0777,file_mode=0777,serverino" >> /etc/fstab'
sudo mount -t cifs //saterraform2.file.core.windows.net/terraform /mnt/terraform -o vers=3.0,credentials=/etc/smbcredentials/saterraform2.cred,dir_mode=0777,file_mode=0777,serverino
```


## MacOS
```
mount_smbfs -d 777 -f 777 //saterraform2:PxAV4itPCxLv8OYUJO1kxEsggempScAicI82bLonvAJSoJj1AuJrD9aHZZ/+jjMzFGXKQEhwrwkxuVuMaGoRrg==@saterraform2.file.core.windows.net terraform

```